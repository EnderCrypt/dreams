/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.activities;


import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import endercrypt.dreams.library.activity.WindowActivity;
import endercrypt.dreams.library.activity.event.method.keyboard.KeyTarget;
import endercrypt.dreams.library.activity.event.type.Event;
import endercrypt.dreams.library.activity.event.type.EventType;
import endercrypt.dreams.utility.TerminalArea;


public class LoginActivity extends WindowActivity
{
	private StringBuilder text = new StringBuilder();
	
	public LoginActivity()
	{
		setTitle("Login");
	}
	
	@Event(EventType.KEYBOARD)
	@KeyTarget()
	public void onType(KeyStroke keyStroke)
	{
		text.append(keyStroke.getCharacter());
	}
	
	@Override
	public TerminalArea getContentArea(TerminalSize size)
	{
		final int width = 25;
		final int height = 2;
		
		int half_x = size.getColumns() / 2;
		int half_y = size.getRows() / 2;
		
		return new TerminalArea(half_x - (width / 2), half_y - (height / 2), width, height);
	}
	
	@Event(EventType.KEYBOARD)
	@KeyTarget(special = KeyType.Backspace)
	public void onErase(KeyStroke keyStroke)
	{
		if (text.length() > 0)
		{
			text.setLength(text.length() - 1);
		}
	}
	
	@Override
	protected void onContentDraw(TextGraphics graphics)
	{
		graphics.putString(0, 0, "Lifetime: " + getLifetime());
		graphics.putString(0, 1, "Text: \"" + text + "\"");
	}
}
