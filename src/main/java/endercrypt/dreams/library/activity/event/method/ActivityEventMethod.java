/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity.event.method;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.library.activity.Activity;
import endercrypt.dreams.library.activity.event.type.IllegalEventSetup;
import javassist.Modifier;


public abstract class ActivityEventMethod<T>
{
	private static final Logger logger = LogManager.getLogger(ActivityEventMethod.class);
	
	protected final Method method;
	protected final Class<T> eventClass;
	
	public ActivityEventMethod(Method method, Class<T> eventClass)
	{
		Objects.requireNonNull(method, "method");
		Objects.requireNonNull(eventClass, "eventClass");
		this.method = method;
		this.eventClass = eventClass;
		
		if (Modifier.isPublic(method.getModifiers()) == false)
		{
			throw new IllegalEventSetup("method " + method + " is not public");
		}
		
		Parameter[] parameters = method.getParameters();
		if (parameters.length != 1)
		{
			throw new IllegalEventSetup("method " + method + " was expected to have 1 argument (" + eventClass.getSimpleName() + ")");
		}
		if (parameters[0].getType().equals(eventClass) == false)
		{
			throw new IllegalEventSetup("method " + method + " expected paramter: " + eventClass);
		}
	}
	
	@SuppressWarnings("unchecked")
	public final boolean validate(Object object)
	{
		Objects.requireNonNull(object, "object");
		if (object.getClass().equals(eventClass) == false)
		{
			return false;
		}
		T casted = (T) object;
		if (validateArgument(casted) == false)
		{
			return false;
		}
		return true;
	}
	
	protected boolean validateArgument(T argument)
	{
		return true;
	}
	
	public void trigger(Activity activity, Object argument)
	{
		Objects.requireNonNull(argument, "argument");
		try
		{
			method.invoke(activity, argument);
		}
		catch (IllegalAccessException e) // shouldnt happen
		{
			e.printStackTrace();
		}
		catch (IllegalArgumentException e)
		{
			logger.catching(e);
			ExitCode.UNCAUGHT_EXCEPTION.endApplication();
		}
		catch (InvocationTargetException e)
		{
			logger.error("Caught exception " + method, e);
			ExitCode.ACTIVITY_FAILURE.endApplication();
		}
	}
}
