/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity.event.method.keyboard;


import java.lang.reflect.Method;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import endercrypt.dreams.library.activity.event.method.ActivityEventMethod;


public class KeyboardEventMethod extends ActivityEventMethod<KeyStroke>
{
	private final KeyTarget keyTarget;
	
	public KeyboardEventMethod(Method method)
	{
		super(method, KeyStroke.class);
		keyTarget = method.getAnnotation(KeyTarget.class);
	}
	
	@Override
	protected boolean validateArgument(KeyStroke argument)
	{
		if (keyTarget != null)
		{
			if (keyTarget.alt() != argument.isAltDown() ||
				keyTarget.ctrl() != argument.isCtrlDown() ||
				keyTarget.shift() != argument.isShiftDown())
			{
				return false;
			}
			if (keyTarget.special().equals(argument.getKeyType()) == false)
			{
				return false;
			}
			if (keyTarget.special() == KeyType.Character &&
				keyTarget.character() != '\0' &&
				keyTarget.character() != argument.getCharacter())
			{
				return false;
			}
		}
		return true;
	}
}
