/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity.event;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import endercrypt.dreams.library.activity.Activity;
import endercrypt.dreams.library.activity.event.method.ActivityEventMethod;
import endercrypt.dreams.library.activity.event.type.Event;
import endercrypt.dreams.library.activity.event.type.EventType;


public class ActivityEventManager
{
	private static Map<Class<? extends Activity>, ActivityEventManager> managers = new HashMap<>();
	
	public static ActivityEventManager generate(Class<? extends Activity> activityClass)
	{
		ActivityEventManager activityEvents = managers.get(activityClass);
		if (activityEvents == null)
		{
			activityEvents = new ActivityEventManager(activityClass);
			managers.put(activityClass, activityEvents);
		}
		return activityEvents;
	}
	
	private List<ActivityEventMethod<?>> eventMethods = new ArrayList<>();
	
	private ActivityEventManager(Class<? extends Activity> activityClass)
	{
		for (Method method : activityClass.getDeclaredMethods())
		{
			Event event = method.getAnnotation(Event.class);
			if (event != null)
			{
				EventType eventType = event.value();
				ActivityEventMethod<?> activityEventMethod = eventType.generate(method);
				eventMethods.add(activityEventMethod);
			}
		}
	}
	
	public void trigger(Activity activity, Object argument)
	{
		for (ActivityEventMethod<?> activityEventMethod : eventMethods)
		{
			if (activityEventMethod.validate(argument))
			{
				activityEventMethod.trigger(activity, argument);
			}
		}
	}
}
