/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.graphics.TextGraphics;

import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.library.DreamsComponent;
import endercrypt.dreams.library.DreamsManager;
import endercrypt.dreams.library.terminal.TerminalManager;
import endercrypt.dreams.utility.DreamsUtility;
import endercrypt.dreams.utility.TerminalArea;


public class ActivityManager extends DreamsComponent
{
	private static final Logger logger = LogManager.getLogger(ActivityManager.class);
	
	public ActivityManager(DreamsManager dreamsManager)
	{
		super(dreamsManager);
	}
	
	private List<Activity> stack = new ArrayList<>();
	
	public void overwrite(Activity context, Activity activity)
	{
		push(activity);
		context.die();
	}
	
	public Activity push(Activity activity)
	{
		logger.info("Activity got pushed: " + activity.getClass().getSimpleName());
		try
		{
			// initialize
			logger.debug("initializing activity");
			stack.add(activity);
			activity.initialize(dreamsManager);
			
			// start
			logger.debug("starting activity");
			activity.start();
			
			// main loop
			logger.debug("runnung activity main loop");
			TerminalManager terminal = getDreamsManager().getTerminalManager();
			while (activity.getLifeCycle() == ActivityLife.STARTED)
			{
				// draw
				logger.trace("activity: draw");
				drawActivities();
				
				// prolong
				logger.trace("activity: prolonging");
				activity.prolong();
				
				// key strokes
				logger.trace("activity: key strokes");
				terminal.getKeyFetcher().fetchGetAll().forEach(activity::triggerKey);
				
				// update
				logger.trace("activity: update");
				activity.update();
			}
			
			// finish
			logger.debug("activity " + activity.getClass().getSimpleName() + " finished");
			terminal.fullClear();
			stack.remove(activity);
			return activity;
		}
		catch (ActivityException e)
		{
			panic(e.getActivity(), e);
			return null;
		}
		catch (Exception e)
		{
			panic(activity, e);
			return null;
		}
	}
	
	public void drawActivities()
	{
		logger.trace("Drawing activities on screen");
		TerminalManager terminalManager = dreamsManager.getTerminalManager();
		terminalManager.performResize();
		
		TextGraphics activityGraphics = terminalManager.createGraphics();
		activityGraphics.fill(' ');
		draw(activityGraphics);
		
		terminalManager.render();
	}
	
	private void panic(Activity activity, Exception e)
	{
		Objects.requireNonNull(activity, "activity");
		Objects.requireNonNull(e, "e");
		logger.error("Caught exception in " + activity.getClass().getSimpleName(), e);
		ExitCode.ACTIVITY_FAILURE.endApplication();
	}
	
	private void draw(TextGraphics graphics)
	{
		TerminalArea area = new TerminalArea(graphics.getSize());
		Iterator<Activity> iterator = stack.iterator();
		while (iterator.hasNext())
		{
			Activity activity = iterator.next();
			logger.trace("Drawing activity: " + activity.getClass().getSimpleName());
			
			// setup
			TerminalArea activityArea = activity.getArea(area.getSize());
			DreamsUtility.validateArea(activityArea, area.getSize());
			TextGraphics activityGraphics = graphics.newTextGraphics(activityArea.getPosition(), activityArea.getSize());
			
			// resize
			TerminalArea activityContainerArea = activity.getContainerArea(area.getSize());
			DreamsUtility.validateArea(activityContainerArea, area.getSize());
			if (iterator.hasNext() && DreamsUtility.isFullscreen(activityContainerArea, area.getSize()))
			{
				continue;
			}
			area = activityContainerArea;
			graphics = graphics.newTextGraphics(activityContainerArea.getPosition(), activityContainerArea.getSize());
			
			// draw
			activity.draw(activityGraphics, iterator.hasNext());
		}
	}
}
