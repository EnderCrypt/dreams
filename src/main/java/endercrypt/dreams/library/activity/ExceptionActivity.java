/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import org.apache.commons.lang3.exception.ExceptionUtils;

import com.googlecode.lanterna.graphics.TextGraphics;


public class ExceptionActivity extends Activity
{
	private final String[] text;
	
	public ExceptionActivity(Throwable exception)
	{
		this.text = ExceptionUtils.getStackTrace(exception).split("\n");
	}
	
	@Override
	protected void onDraw(TextGraphics graphics)
	{
		int x = 1;
		for (int i = 0; i < text.length; i++)
		{
			int y = 1 + i;
			String line = text[i];
			graphics.putString(x, y, line);
		}
	}
}
