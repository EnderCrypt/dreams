/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import endercrypt.dreams.utility.AsciiGraphics;
import endercrypt.dreams.utility.AsciiLines.Type;
import endercrypt.dreams.utility.DreamsUtility;
import endercrypt.dreams.utility.TerminalArea;


public abstract class WindowActivity extends Activity
{
	private String title = "Untitled";
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	@Override
	public final TerminalArea getArea(TerminalSize size)
	{
		TerminalSize contentSize = new TerminalSize(size.getColumns() - 2, size.getRows() - 2);
		TerminalArea contentArea = getContentArea(contentSize);
		DreamsUtility.validateArea(contentArea, contentSize);
		return new TerminalArea(contentArea.x, contentArea.y, contentArea.width + 2, contentArea.height + 2);
	}
	
	@Override
	public final TerminalArea getContainerArea(TerminalSize size)
	{
		TerminalSize contentSize = new TerminalSize(size.getColumns() - 2, size.getRows() - 2);
		TerminalArea contentArea = getContentArea(contentSize);
		DreamsUtility.validateArea(contentArea, contentSize);
		return new TerminalArea(1 + contentArea.x, 1 + contentArea.y, contentArea.width, contentArea.height);
	}
	
	public TerminalArea getContentArea(TerminalSize size)
	{
		return new TerminalArea(size.getColumns(), size.getRows());
	}
	
	@Override
	protected void onDraw(TextGraphics graphics)
	{
		TerminalSize size = graphics.getSize();
		AsciiGraphics.rectangle(graphics, 0, 0, size.getColumns() - 1, size.getRows() - 1, Type.DOUBLE, title);
		
		TerminalPosition contentPosition = new TerminalPosition(1, 1);
		TerminalSize contentSize = new TerminalSize(size.getColumns() - 2, size.getRows() - 2);
		TextGraphics contentGraphics = graphics.newTextGraphics(contentPosition, contentSize);
		onContentDraw(contentGraphics);
	}
	
	protected abstract void onContentDraw(TextGraphics graphics);
}
