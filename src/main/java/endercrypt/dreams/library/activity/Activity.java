/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.activity;


import java.io.IOException;
import java.util.Objects;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalResizeListener;

import endercrypt.dreams.library.DreamsManager;
import endercrypt.dreams.library.activity.event.ActivityEventManager;
import endercrypt.dreams.library.terminal.TerminalManager;
import endercrypt.dreams.utility.TerminalArea;


public abstract class Activity
{
	// EVENTS //
	
	private ActivityEventManager events = ActivityEventManager.generate(getClass());
	
	void triggerEvent(Object eventArgument)
	{
		events.trigger(this, eventArgument);
	}
	
	// DREAMS MANAGER //
	
	private DreamsManager dreamsManager;
	
	void initialize(DreamsManager dreamsManager)
	{
		Objects.requireNonNull(dreamsManager, "dreamsManager");
		switchLifeCycle(ActivityLife.UNBORN, ActivityLife.INITIALIZED);
		this.dreamsManager = dreamsManager;
	}
	
	protected DreamsManager getDreamsManager()
	{
		requireLifeCycle(ActivityLife.STARTED);
		return dreamsManager;
	}
	
	// KEYSTROKES //
	
	public void triggerKey(KeyStroke key)
	{
		triggerEvent(key);
	}
	
	// LIFECYCLE //
	
	private ActivityLife lifeCycle = ActivityLife.UNBORN;
	
	public ActivityLife getLifeCycle()
	{
		return lifeCycle;
	}
	
	private void switchLifeCycle(ActivityLife expected, ActivityLife target)
	{
		if (expected != null)
		{
			requireLifeCycle(expected);
		}
		lifeCycle = target;
	}
	
	private void requireLifeCycle(ActivityLife expected)
	{
		if (lifeCycle != expected)
		{
			panic("requireLifeCycle", new IllegalStateException("expected " + ActivityLife.class.getSimpleName() + "." + expected));
		}
	}
	
	private void panic(String method, Throwable e)
	{
		switchLifeCycle(null, ActivityLife.DEAD);
		throw new ActivityException(this, method, e);
	}
	
	// LIFETIME //
	private int lifetime = 0;
	
	public int getLifetime()
	{
		return lifetime;
	}
	
	// START //
	
	void start()
	{
		switchLifeCycle(ActivityLife.INITIALIZED, ActivityLife.STARTED);
		try
		{
			onStart();
		}
		catch (Exception e)
		{
			panic("start", e);
		}
	}
	
	protected void onStart()
	{
		
	}
	
	// DEATH //
	
	protected void die()
	{
		switchLifeCycle(ActivityLife.STARTED, ActivityLife.DEAD);
		try
		{
			onDeath();
		}
		catch (Exception e)
		{
			panic("death", e);
		}
	}
	
	protected void onDeath()
	{
		
	}
	
	// UPDATE //
	
	private TerminalResizeListener terminalResizeListener = new TerminalResizeListener()
	{
		@Override
		public void onResized(Terminal terminal, TerminalSize newSize)
		{
			getDreamsManager().getActivityManager().drawActivities();
		}
	};
	
	public void prolong() throws IOException
	{
		TerminalManager terminalManager = getDreamsManager().getTerminalManager();
		Terminal terminal = terminalManager.getTerminal();
		
		terminal.addResizeListener(terminalResizeListener);
		terminalManager.getKeyFetcher().blockingGetAll().forEach(this::triggerKey);
		terminal.removeResizeListener(terminalResizeListener);
	}
	
	void update()
	{
		requireLifeCycle(ActivityLife.STARTED);
		lifetime++;
		try
		{
			onUpdate();
		}
		catch (Exception e)
		{
			panic("update", e);
		}
	}
	
	protected void onUpdate()
	{
		
	}
	
	// DRAW //
	
	void draw(TextGraphics graphics, boolean background)
	{
		requireLifeCycle(ActivityLife.STARTED);
		try
		{
			if (background)
			{
				onDrawBackground(graphics);
			}
			else
			{
				onDraw(graphics);
			}
		}
		catch (Exception e)
		{
			panic("draw", e);
		}
	}
	
	public TerminalArea getArea(TerminalSize size)
	{
		return new TerminalArea(size);
	}
	
	public TerminalArea getContainerArea(TerminalSize size)
	{
		return new TerminalArea(size);
	}
	
	protected void onDrawBackground(TextGraphics graphics)
	{
		onDraw(graphics);
	}
	
	protected abstract void onDraw(TextGraphics graphics);
}
