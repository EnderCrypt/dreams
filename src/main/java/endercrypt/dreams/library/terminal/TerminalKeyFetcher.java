/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.library.terminal;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.Terminal;

import endercrypt.dreams.application.ExitCode;


public class TerminalKeyFetcher
{
	private static ExecutorService executor = new ThreadPoolExecutor(1, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
	
	private static final Logger logger = LogManager.getLogger(TerminalKeyFetcher.class);
	
	private Terminal terminal;
	
	public TerminalKeyFetcher(Terminal terminal)
	{
		Objects.requireNonNull(terminal, "terminal");
		this.terminal = terminal;
	}
	
	// TIMED BLOCKING //
	
	public KeyStroke blocking(int ms)
	{
		try
		{
			Future<KeyStroke> future = executor.invokeAll(List.of(new Callable<KeyStroke>()
			{
				@Override
				public KeyStroke call() throws Exception
				{
					return blocking();
				}
			}), ms, TimeUnit.MILLISECONDS).get(0);
			return future.get();
		}
		catch (InterruptedException e)
		{
			return null;
		}
		catch (ExecutionException e)
		{
			logger.catching(e);
			ExitCode.UNCAUGHT_EXCEPTION.endApplication();
			return null;
		}
	}
	
	public List<KeyStroke> blockingGetAll(int ms) throws IOException
	{
		List<KeyStroke> keyStrokes = new ArrayList<>();
		keyStrokes.add(blocking(ms));
		keyStrokes.addAll(fetchGetAll());
		return keyStrokes;
	}
	
	// BLOCKING //
	
	public KeyStroke blocking() throws IOException
	{
		return filter(terminal.readInput());
	}
	
	public List<KeyStroke> blockingGetAll() throws IOException
	{
		List<KeyStroke> keyStrokes = new ArrayList<>();
		keyStrokes.add(blocking());
		keyStrokes.addAll(fetchGetAll());
		return keyStrokes;
	}
	
	// FETCH //
	
	public KeyStroke fetch() throws IOException
	{
		return filter(terminal.pollInput());
	}
	
	public List<KeyStroke> fetchGetAll() throws IOException
	{
		List<KeyStroke> keyStrokes = new ArrayList<KeyStroke>();
		KeyStroke keyStroke;
		while ((keyStroke = fetch()) != null)
		{
			keyStrokes.add(keyStroke);
		}
		return keyStrokes;
	}
	
	// FILTER
	
	protected KeyStroke filter(KeyStroke key)
	{
		if (key == null)
		{
			return null;
		}
		logger.trace("Collected keystroke: " + key);
		if (key.isCtrlDown() && key.getCharacter() == 'c')
		{
			logger.info("Caught CTRL-C, terminating");
			ExitCode.SUCCESS.endApplication();
			return null;
		}
		return key;
	}
}
