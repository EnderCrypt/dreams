/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.dreams.activities.ApplicationActivity;
import endercrypt.dreams.application.ExitCode;
import endercrypt.dreams.application.LaunchArguments;
import endercrypt.dreams.library.DreamsManager;


public class DreamsMain
{
	private static final Logger logger = LogManager.getLogger(DreamsMain.class);
	
	public static void main(LaunchArguments arguments)
	{
		logger.info("Starting dreams manager ...");
		DreamsManager dreamsManager = new DreamsManager(arguments);
		logger.info("Launching splash screen");
		dreamsManager.getActivityManager()
			.push(new ApplicationActivity());
		
		logger.info("Run shutdown sequence ...");
		dreamsManager.getTerminalManager().shutdown();
		
		logger.info("Application terminated successfully");
		ExitCode.SUCCESS.endApplication();
	}
}
