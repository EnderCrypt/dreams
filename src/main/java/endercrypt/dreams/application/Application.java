/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.application;


import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import endercrypt.dreams.DreamsMain;


public class Application
{
	private static final Logger logger = LogManager.getLogger(Application.class);
	
	public static final File filename = new File(Application.class.getProtectionDomain().getCodeSource().getLocation().getPath());
	
	public static final String name = "Dreams";
	public static final String author = "EnderCrypt";
	public static final Version version = new Version(0, 0, 1);
	
	public static void main(String[] args) throws Exception
	{
		// launch arguments
		LaunchArguments launchArguments = new LaunchArguments();
		CmdLineParser parser = new CmdLineParser(launchArguments);
		try
		{
			parser.parseArgument(args);
		}
		catch (CmdLineException e)
		{
			System.err.println("Error: " + e.getMessage());
			System.err.println("java -jar " + filename.getName() + " [Options] [Files ...]");
			new CmdLineParser(new LaunchArguments()).printUsage(System.err);
			ExitCode.BAD_CMD_ARGS.endApplication();
		}
		
		// print version
		if (launchArguments.isVersion())
		{
			System.out.println(Application.class.getSimpleName() + " by " + Application.author + " Version " + Application.version);
			ExitCode.SUCCESS.endApplication();
		}
		
		// print help
		if (launchArguments.isHelp())
		{
			System.out.println("java -jar " + Application.filename.getName() + " [Options] [Files ...]");
			new CmdLineParser(new LaunchArguments()).printUsage(System.out);
			System.out.println();
			System.out.println("Exit Codes: ");
			List<ExitCode> exitCodes = ExitCode.getExitCodes().stream().sorted((e1, e2) -> Integer.compare(e1.getCode(), e2.getCode())).collect(Collectors.toList());
			for (ExitCode exitCode : exitCodes)
			{
				System.out.println("\t" + (exitCode.getCode() >= 0 ? " " : "") + exitCode.getCode() + " = " + exitCode.getDescription());
			}
			ExitCode.SUCCESS.endApplication();
		}
		
		// info
		logger.info(Application.name + " " + Application.version);
		
		// uncaught exception
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler()
		{
			@Override
			public void uncaughtException(Thread thread, Throwable exception)
			{
				String threadName = thread.getName();
				
				logger.error("Thread " + threadName + " crashed with uncaught exception", exception);
				ExitCode.UNCAUGHT_EXCEPTION.endApplication();
				return;
			}
		});
		
		// Main
		DreamsMain.main(launchArguments);
	}
}
