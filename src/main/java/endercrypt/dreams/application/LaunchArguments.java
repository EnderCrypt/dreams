/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.application;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;


public class LaunchArguments
{
	protected LaunchArguments()
	{
		
	}
	
	@Option(name = "-version", aliases = { "--version", "-v" }, required = false, usage = "prints the version number rather than perform anything")
	private boolean version = false;
	
	public boolean isVersion()
	{
		return version;
	}
	
	@Option(name = "-help", aliases = { "--help", "-h" }, required = false, usage = "prints the help information rather than perform anything")
	private boolean help = false;
	
	public boolean isHelp()
	{
		return help;
	}
	
	@Argument
	private List<String> arguments = new ArrayList<>();
	
	public List<String> getArguments()
	{
		return Collections.unmodifiableList(arguments);
	}
}
