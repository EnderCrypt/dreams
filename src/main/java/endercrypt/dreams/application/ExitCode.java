/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.application;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.dreams.utility.DreamsUtility;


public class ExitCode implements Comparable<ExitCode>
{
	private static final Logger logger = LogManager.getLogger(ExitCode.class);
	
	private static List<ExitCode> exitCodes = new ArrayList<>();
	
	public static final ExitCode SUCCESS = new ExitCode("The command executed successfully");
	
	public static final ExitCode UNIX_GENERAL_ERROR = new ExitCode("(UNIX) Catchall for general errors");
	public static final ExitCode UNIX_MISUSE_SHELL = new ExitCode("(UNIX) Misuse of shell builtins");
	
	public static final ExitCode BAD_CMD_ARGS = new ExitCode("Bad command-line arguments");
	public static final ExitCode UNCAUGHT_EXCEPTION = new ExitCode("Uncaught exception");
	public static final ExitCode TERMINAL_ERROR = new ExitCode("Terminal communication failure");
	public static final ExitCode ACTIVITY_FAILURE = new ExitCode("Activity management failure");
	public static final ExitCode RESOURCE_FAILURE = new ExitCode("Reading internal resources failed");
	
	static
	{
		Collections.sort(exitCodes);
	}
	
	private static int failure_code_counter = 0;
	
	public static List<ExitCode> getExitCodes()
	{
		return Collections.unmodifiableList(exitCodes);
	}
	
	private int code;
	private String description;
	
	private ExitCode(String description)
	{
		this(failure_code_counter++, description);
	}
	
	private ExitCode(int code, String description)
	{
		this.code = code;
		this.description = DreamsUtility.capitalize(description);
		
		exitCodes.add(this);
	}
	
	public int getCode()
	{
		return code;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	@Override
	public int compareTo(ExitCode other)
	{
		return -Integer.compare(this.getCode(), other.getCode());
	}
	
	public void endApplication()
	{
		logger.info("Exit: ( " + getCode() + " ) " + getDescription());
		System.exit(getCode());
	}
}
