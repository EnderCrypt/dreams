package endercrypt.dreams.utility;


/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.dreams.application.ExitCode;


public class AsciiLines
{
	private static final Logger logger = LogManager.getLogger(AsciiLines.Type.class);
	
	public static enum Type
	{
		EMPTY, SMALL, BIG, DOUBLE;
		
		public static Type interpret(char key)
		{
			Objects.requireNonNull(key, "key");
			String keyString = String.valueOf(Character.toUpperCase(key));
			for (Type type : values())
			{
				if (type.name().startsWith(keyString))
				{
					return type;
				}
			}
			throw new IllegalArgumentException("failed to interpret: " + key);
		}
	}
	
	private static final String resourceFile = "ascii_lines.txt";
	private static char[] chars;
	
	static
	{
		try (BufferedInputStream input = new BufferedInputStream(AsciiLines.class.getResourceAsStream("/" + resourceFile)))
		{
			Map<Integer, Character> charMap = new HashMap<>();
			String content = new String(input.readAllBytes());
			for (String line : content.split("\n"))
			{
				if (line.length() == 0)
				{
					continue;
				}
				int equalIndex = line.indexOf('=');
				String keyString = line.substring(0, equalIndex).strip();
				// key
				if (keyString.length() != 4)
				{
					throw new IllegalArgumentException("illegal key: " + line);
				}
				// value
				String valueString = line.substring(equalIndex + 1).strip();
				char[] values = valueString.toCharArray();
				if (values.length != 1)
				{
					throw new IllegalArgumentException("illegal value: " + line);
				}
				char character = values[0];
				// interpret
				Type[] keyTypes = keyString.chars().mapToObj(i -> Type.interpret((char) i)).toArray(Type[]::new);
				int index = index(keyTypes);
				charMap.put(index, character);
			}
			
			chars = new char[charMap.keySet().stream().mapToInt(i -> i).max().getAsInt() + 1];
			for (Entry<Integer, Character> entry : charMap.entrySet())
			{
				chars[entry.getKey()] = entry.getValue();
			}
			logger.info("read " + charMap.size() + " characters from " + resourceFile);
		}
		catch (IOException e)
		{
			logger.error("failed to read box.txt from resources", e);
			ExitCode.RESOURCE_FAILURE.endApplication();
		}
	}
	
	private static int index(Type... types)
	{
		int index = 0;
		for (int i = 0; i < types.length; i++)
		{
			Type type = types[i];
			int mult = (int) Math.pow(types.length, i);
			int value = type.ordinal() * mult;
			// System.out.println("ordinal: (" + type.toString() + ") " + type.ordinal() + " value: " + value + " mult: " + mult);
			index += value;
		}
		// System.out.println("index: " + index);
		return index;
	}
	
	public static char get(Type up, Type down, Type left, Type right)
	{
		int index = index(up, down, left, right);
		char c = '\0';
		if (index < chars.length)
		{
			c = chars[index];
		}
		if (c == '\0')
		{
			throw new IllegalArgumentException(up + ", " + down + ", " + left + ", " + right + " is unavailable or missing");
		}
		return c;
	}
}
