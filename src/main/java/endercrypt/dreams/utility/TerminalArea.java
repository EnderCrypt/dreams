/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.utility;


import java.awt.Rectangle;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;


public class TerminalArea
{
	public final int x, y;
	public final int width, height;
	
	public TerminalArea(Rectangle rectangle)
	{
		this(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
	}
	
	public TerminalArea(TerminalPosition position, TerminalSize size)
	{
		this(position.getRow(), position.getColumn(), size.getColumns(), size.getRows());
	}
	
	public TerminalArea(TerminalSize size)
	{
		this(size.getColumns(), size.getRows());
	}
	
	public TerminalArea(int width, int height)
	{
		this(0, 0, width, height);
	}
	
	public TerminalArea(int x, int y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public TerminalPosition getPosition()
	{
		return new TerminalPosition(x, y);
	}
	
	public TerminalSize getSize()
	{
		return new TerminalSize(width, height);
	}
	
	@Override
	public String toString()
	{
		return "TerminalArea [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + "]";
	}
}
