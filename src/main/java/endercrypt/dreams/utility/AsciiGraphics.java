/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.utility;


import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.graphics.TextGraphics;

import endercrypt.dreams.utility.AsciiLines.Type;


public class AsciiGraphics
{
	public static void vertical(TextGraphics graphics, int x, int min, int max, boolean continual, Type type)
	{
		for (int y = min; y <= max; y++)
		{
			Type type_up = y == min && continual == false ? Type.EMPTY : type;
			Type type_down = y == max && continual == false ? Type.EMPTY : type;
			char c = AsciiLines.get(type_up, type_down, Type.EMPTY, Type.EMPTY);
			graphics.setCharacter(x, y, c);
		}
	}
	
	public static void horizontal(TextGraphics graphics, int y, int min, int max, boolean continual, Type type)
	{
		for (int x = min; x <= max; x++)
		{
			Type type_left = x == min && continual == false ? Type.EMPTY : type;
			Type type_right = x == max && continual == false ? Type.EMPTY : type;
			char c = AsciiLines.get(Type.EMPTY, Type.EMPTY, type_left, type_right);
			graphics.setCharacter(x, y, c);
		}
	}
	
	public static void rectangle(TextGraphics graphics, int x1, int y1, int x2, int y2, Type type)
	{
		vertical(graphics, x1, y1, y2, true, type);
		vertical(graphics, x2, y1, y2, true, type);
		
		horizontal(graphics, y1, x1, x2, true, type);
		horizontal(graphics, y2, x1, x2, true, type);
		
		graphics.setCharacter(x1, y1, AsciiLines.get(Type.EMPTY, type, Type.EMPTY, type));
		graphics.setCharacter(x2, y1, AsciiLines.get(Type.EMPTY, type, type, Type.EMPTY));
		graphics.setCharacter(x1, y2, AsciiLines.get(type, Type.EMPTY, Type.EMPTY, type));
		graphics.setCharacter(x2, y2, AsciiLines.get(type, Type.EMPTY, type, Type.EMPTY));
	}
	
	public static void rectangle(TextGraphics graphics, int x1, int y1, int x2, int y2, Type type, String label)
	{
		rectangle(graphics, x1, y1, x2, y2, type, label, null);
	}
	
	public static void rectangle(TextGraphics graphics, int x1, int y1, int x2, int y2, Type type, String label, Type edge)
	{
		rectangle(graphics, x1, y1, x2, y2, type);
		
		label = "[ " + label + " ]";
		
		int center = (x2 - x1) / 2;
		int start = center - (label.length() / 2);
		graphics.enableModifiers(SGR.REVERSE);
		graphics.putString(start, y1, label);
		
		if (edge != null)
		{
			graphics.setCharacter(start - 1, y1, AsciiLines.get(edge, edge, type, Type.EMPTY));
			graphics.setCharacter(start + label.length(), y1, AsciiLines.get(edge, edge, Type.EMPTY, type));
		}
		graphics.disableModifiers(SGR.REVERSE);
	}
}
