/************************************************************************
 * Dreams by EnderCrypt                                                 *
 * Copyright (C) 2020                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.dreams.utility;


import java.awt.Rectangle;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;


public class DreamsUtility
{
	public static final ExecutorService executorService = Executors.newCachedThreadPool();
	
	public static double randomRange(double min, double max)
	{
		return min + (Math.random() * (max - min));
	}
	
	public static String repeat(int times, char c)
	{
		return repeat(times, String.valueOf(c));
	}
	
	public static String repeat(int times, String text)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++)
		{
			sb.append(text);
		}
		return sb.toString();
	}
	
	public static String capitalize(String description)
	{
		if (description == null || description.contentEquals(""))
		{
			return description;
		}
		return Character.toUpperCase(description.charAt(0)) + description.substring(1);
	}
	
	public static <T> T randomEntry(List<T> list)
	{
		int index = (int) Math.floor(Math.random() * list.size());
		return list.get(index);
	}
	
	public static void validateArea(TerminalArea area, TerminalSize size)
	{
		if (area.x < 0 || area.y < 0 || area.width > size.getColumns() || area.height > size.getRows())
		{
			throw new IllegalArgumentException("Rectangle " + area + " does not match whitin " + size);
		}
	}
	
	public static boolean isFullscreen(TerminalArea area, TerminalSize size)
	{
		validateArea(area, size);
		return (area.x == 0 || area.y == 0 || area.width == size.getColumns() || area.height == size.getRows());
	}
	
	public static TerminalPosition asPosition(Rectangle rectangle)
	{
		return new TerminalPosition(rectangle.x, rectangle.y);
	}
	
	public static TerminalSize asSize(Rectangle rectangle)
	{
		return new TerminalSize(rectangle.width, rectangle.height);
	}
}
